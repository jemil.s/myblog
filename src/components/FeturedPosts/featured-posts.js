import React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import FeaturedPost from '../FeaturedPost/featured-post';
import * as style from './featuredPosts.module.css';

const FeaturedPostsList = () => {
  const data = useStaticQuery(graphql`
    query {
      allDatoCmsArticle {
        nodes {
          author {
            avatar {
              url
            }
            name
          }
          body
          tags {
            name
          }
          slug
          title
          featured
          id
        }
      }
    }
  `);

  return (
    <div>
      <h3 className="title">Featured posts</h3>
      <ul className={style.featuredPosts}>
        {data.allDatoCmsArticle.nodes.map((node) => {
          if (node.featured) {
            return <FeaturedPost key={node.id} {...node} />;
          }
        })}
      </ul>
    </div>
  );
};

export default FeaturedPostsList;
