import React from 'react';
import { graphql, Link } from 'gatsby';
import * as style from './blogList.module.css';
import Layout from '../components/Layout/layout';
import Post from '../components/Post/post';
import FeaturedPostsList from '../components/FeturedPosts/featured-posts';

const BlogList = ({ data, pageContext }) => {
  const articles = data.allDatoCmsArticle.nodes;
  const { currentPage, numPages } = pageContext;

  const prevPagePath = currentPage === 2 ? '/' : `/${currentPage - 1}`;
  const nextPagePath = `/${currentPage + 1}`;

  return (
    <div>
      <Layout>
        <div className={style.featured}>
          <FeaturedPostsList />
        </div>
        <h3 className="title">All posts</h3>
        <div className={style.posts}>
          {articles.map((article) => {
            return <Post key={article.id} {...article} />;
          })}
        </div>
        <div className={style.paginationBar}>
          <Link className={currentPage === 1 ? style.disabledButton : style.button} to={prevPagePath}>
            prev
          </Link>
          {Array.from({ length: numPages }).map((_, index) => {
            const pageNumber = index + 1;
            const goToPageNumber = (number) => {
              if (number === 1) {
                return `/`;
              }
              return `/${number}`;
            };
            return (
              <Link
                to={goToPageNumber(pageNumber)}
                key={pageNumber}
                className={`${style.pageNumber} ${currentPage === pageNumber && style.activePage}`}
              >
                {pageNumber}
              </Link>
            );
          })}
          <Link className={currentPage === numPages ? style.disabledButton : style.button} to={nextPagePath}>
            next
          </Link>
        </div>
      </Layout>
    </div>
  );
};

export default BlogList;

export const query = graphql`
  query ($limit: Int!, $skip: Int!) {
    allDatoCmsArticle(limit: $limit, skip: $skip) {
      nodes {
        author {
          avatar {
            url
          }
          name
        }
        body
        tags {
          name
        }
        slug
        title
        featured
        id
      }
    }
  }
`;
