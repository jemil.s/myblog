import { Link, useStaticQuery, graphql } from 'gatsby';
import React, { useState } from 'react';
import * as style from './dropdown.module.css';

const Dropdown = () => {
  const [open, setOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState('All');

  const data = useStaticQuery(graphql`
    query {
      allDatoCmsTag {
        nodes {
          name
        }
        totalCount
      }
    }
  `);

  const tags = data.allDatoCmsTag.nodes;

  const onOptionSelect = (option) => {
    setSelectedOption(option);
    setOpen(false);
  };

  const toggleOpen = () => setOpen(!open);

  return (
    <div className={style.container}>
      <div onClick={toggleOpen} className={style.header}>
        {selectedOption}
      </div>
      {open && (
        <div>
          <ul className={style.list}>
            <Link to={`/`}>
              <li className={style.listItem} onClick={() => onOptionSelect('All')} key={'All'}>
                All
              </li>
            </Link>
            {tags.length !== 0 &&
              tags.map((tag) => {
                return (
                  <Link key={tag.name} to={`/tags/${tag.name}`}>
                    <li className={style.listItem} onClick={() => onOptionSelect(tag.name)}>
                      {tag.name}
                    </li>
                  </Link>
                );
              })}
          </ul>
        </div>
      )}
    </div>
  );
};

export default Dropdown;
