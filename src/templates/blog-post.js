import React from 'react';
import { graphql, Link } from 'gatsby';
import Layout from '../components/Layout/layout';
import * as style from './blogPost.module.css';
import Author from '../components/Author/author';
import Tags from '../components/Tags/Tags';
const BlogPost = ({ data }) => {

  return (
    <Layout>
      <div className={style.postContainer}>
        <div className={style.header}>
          <h3 className={style.title}>{data.datoCmsArticle.title}</h3>
          <Author author={data.datoCmsArticle.author} />
        </div>
        <p className={style.articleText}>{data.datoCmsArticle.body}</p>
        {data.datoCmsArticle.tags.length !== 0 && (
          <div className={style.tags}>
            <span>tags:</span>
            <Tags tags={data.datoCmsArticle.tags} />
          </div>
        )}
      </div>
    </Layout>
  );
};

export const query = graphql`
  query ($slug: String) {
    datoCmsArticle(slug: { eq: $slug }) {
      body
      title
      tags {
        name
      }
      author {
        name
        avatar {
          url
        }
      }
    }
  }
`;

export default BlogPost;
