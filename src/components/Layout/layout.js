import React from 'react';
import Dropdown from '../Dropdown/dropdown';
import * as style from './layout.module.css';
import { Link } from 'gatsby';
const Layout = ({ children }) => {
  return (
    <div className={style.container}>
      <div className={style.headerWrapper}>
        <Link to="/">
          <h1 className={style.title}>My fancy blog</h1>
        </Link>
        <div>
          <Dropdown />
        </div>
      </div>
      {children}
    </div>
  );
};

export default Layout;
