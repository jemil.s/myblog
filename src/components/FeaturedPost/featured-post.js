import React from 'react';
import * as style from './featuredPost.module.css';
import { Link } from 'gatsby';
import Tags from '../Tags/Tags';
const FeaturedPost = ({ author, tags, title, slug }) => {
  return (
    <div className={style.post}>
      <p className={style.author}>
        <span className={style.textmuted}>by&nbsp;</span>
        {author.name}
      </p>
      <Link to={`${slug}`}>
        <p className={style.title}>{title}</p>
      </Link>
      <div>
        <div className={style.footer}>
          <Tags tags={tags} />
        </div>
      </div>
    </div>
  );
};

export default FeaturedPost;
