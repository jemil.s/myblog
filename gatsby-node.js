const path = require(`path`);

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const result = await graphql(`
    query {
      allDatoCmsArticle(sort: {order: DESC, fields: meta___createdAt}) {
        nodes {
          slug
        }
      }
      allDatoCmsTag {
        nodes {
          name
        }
      }
    }
  `);
  const articles = result.data.allDatoCmsArticle.nodes;
  const allTags = result.data.allDatoCmsTag.nodes;

  articles.forEach((node) => {
    createPage({
      path: node.slug,
      component: path.resolve(`./src/templates/blog-post.js`),
      context: {
        slug: node.slug,
      },
    });
  });

  const articlesPerPage = 2;
  const numPages = Math.ceil(articles.length / articlesPerPage);
  Array.from({ length: numPages }).forEach((_, i) => {
    createPage({
      path: i === 0 ? `/` : `/${i + 1}`,
      component: path.resolve('./src/templates/blog-list.js'),
      context: {
        limit: articlesPerPage,
        skip: i * articlesPerPage,
        numPages,
        currentPage: i + 1,
      },
    });
  });

  allTags.forEach((tag) => {
    createPage({
      path: `/tags/${tag.name}`,
      component: path.resolve(`./src/templates/tags.js`),
      context: {
        tag: tag.name,
      },
    });
  });
};
