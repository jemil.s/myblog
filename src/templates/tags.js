import React from 'react';
import Layout from '../components/Layout/layout';
import * as style from './tags.module.css';
import { graphql } from 'gatsby';
import Post from '../components/Post/post';

const Tags = ({ data, pageContext }) => {
  const { tag } = pageContext;
  const posts = data.allDatoCmsArticle.nodes;
  return (
    <div>
      <Layout>
        <div className={style.heading}>
          <h3 className="title">
            All posts by <span className={style.tag}>{tag}</span> tag
          </h3>
        </div>
        <div className={style.posts}>
          {posts.length === 0 ? (
            <div>No posts by this tag</div>
          ) : (
            posts.map((post) => {
              return <Post key={post.id} {...post} />;
            })
          )}
        </div>
      </Layout>
    </div>
  );
};

export default Tags;

export const query = graphql`
  query ($tag: String) {
    allDatoCmsArticle(filter: { tags: { elemMatch: { name: { eq: $tag } } } }) {
      nodes {
        title
        author {
          avatar {
            url
          }
          name
        }
        id
        slug
        tags {
          name
        }
      }
    }
  }
`;
