import React from 'react';
import * as style from './tags.module.css';
import { Link } from 'gatsby';
const Tags = ({ tags }) => {
  return (
    <span>
      {tags.map((tag) => {
        return (
          <span key={tag.name} className={style.tag}>
            <Link to={`/tags/${tag.name}`}>
              <span>{tag.name}</span>
            </Link>
          </span>
        );
      })}
    </span>
  );
};

export default Tags;
