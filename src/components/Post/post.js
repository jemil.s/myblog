import React from 'react';
import * as style from './post.module.css';
import { Link } from 'gatsby';
import Author from '../Author/author';
import Tags from '../Tags/Tags';
const Post = ({ author, tags, title, slug }) => {
  return (
    <div className={style.wrapper}>
      <Link to={`/${slug}`}>
        <span className={style.title}>{title}</span>
      </Link>
      <div className={style.postFooter}>
        <Author author={author} />
        {tags.length !== 0 && <Tags tags={tags} />}
      </div>
    </div>
  );
};

export default Post;
