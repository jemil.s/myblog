import React from 'react';
import * as style from './author.module.css';

const Author = ({ author }) => {
  return (
    <div className={style.author}>
      <div>{author.avatar ? <img className={style.avatar} src={author.avatar.url} /> : ''}</div>
      <p>{author.name}</p>
    </div>
  );
};

export default Author;
